#  NoMAD AD Auth Sample

This is a quick example of how to use the NoMAD-ADAuth Framework in a Swift project.

Almost all of the action happens in ViewController.swift where the NoMADSession is created, the user input is received from the UI and then the user is attempted to authenticate. On successful authentication the user's password expiration date is returned as a modal alert.

