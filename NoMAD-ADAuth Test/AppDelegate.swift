//
//  AppDelegate.swift
//  NoMAD-ADAuth Test
//
//  Created by Joel Rennich on 11/10/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

