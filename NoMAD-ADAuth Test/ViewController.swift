//
//  ViewController.swift
//  NoMAD-ADAuth Test
//
//  Created by Joel Rennich on 11/10/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Cocoa
import NoMAD_ADAuth

class ViewController: NSViewController {
    
    // IB outlets
    @IBOutlet weak var domainField: NSTextField!
    @IBOutlet weak var userField: NSTextField!
    @IBOutlet weak var passField: NSSecureTextField!
    @IBOutlet weak var doItButton: NSButton!
    @IBOutlet weak var spinner: NSProgressIndicator!
    
    // globals
    var session: NoMADSession? = nil
    let backgroundQueue = DispatchQueue(label: "menu.nomad.NoMADExample.background", attributes: [])

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NSApp.windows[0].center()
        NSApp.windows[0].orderFront(nil)
    }
    
    override func viewWillDisappear() {
        stopUI()
        session = nil
        print("going away")
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    //MARK: IB Actions
    @IBAction func doItClick(_ sender: Any) {
        
        // get all the info and get things going
        session = NoMADSession.init(domain: domainField.stringValue, user: userField.stringValue, type: .AD)
        
        session?.userPass = passField.stringValue

        // make sure to set the delegate to your self
        session?.delegate = self
        
        startUI()
        
        // authenticate in the background
        backgroundQueue.async {
            self.session?.authenticate()
        }
    }
    
    //MARK: UI functions
    func startUI() {
        // move this back to the foreground for UI
        RunLoop.main.perform {
            
            // start the UI spinning
            self.spinner.isHidden = false
            self.spinner.startAnimation(nil)
            self.doItButton.isEnabled = false
        }
    }
    
    func stopUI() {
        // move this back to the foreground for UI
        RunLoop.main.perform {
            
            // stop the UI
            self.spinner.isHidden = true
            self.spinner.stopAnimation(nil)
            self.doItButton.isEnabled = true
        }
    }
}

class WindowController : NSWindowController, NSWindowDelegate {
    
    func windowWillClose(_ notification: Notification) {
        // be nice and close the app when the window goes
        NSApp.terminate(nil)
    }
}

//MARK: NoMADUserSession Callbacks
extension ViewController: NoMADUserSessionDelegate {
    func NoMADAuthenticationSucceded() {
        print("Auth Succeeded")
        session?.userInfo()
    }

    func NoMADAuthenticationFailed(error: Error, description: String) {
        stopUI()

        // move this back to the foreground for UI
        RunLoop.main.perform {
            let myAlert = NSAlert()
            myAlert.messageText = description
            myAlert.beginSheetModal(for: NSApp.windows.first!, completionHandler: nil)
        }
    }

    func NoMADUserInformation(user: ADUserRecord) {
        stopUI()
        let expirationDate = user.passwordExpire
        var message = "Password does not expire."
        if expirationDate != nil && user.passwordAging! {
            let daysToGo = String(Int((expirationDate?.timeIntervalSince(Date()))!)/86400)
            message = "Password expires in \(daysToGo) days."
        }

        // move this back to the foreground for UI
        RunLoop.main.perform {
            let myAlert = NSAlert()
            myAlert.messageText = message
            myAlert.beginSheetModal(for: NSApp.windows.first!, completionHandler: nil)
        }
        session = nil
    }
}
